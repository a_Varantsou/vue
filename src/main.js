import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import Vuetify from 'vuetify';
import URLSearchParams from 'url-search-params';
import store from './store';
import { http, router } from './http';
import auth from './auth';
import App from './app.vue';
import Appbar from './components/app-bar.vue';
import Appfooter from './components/app-footer.vue';

Vue.config.productionTip = false;

// Polyfills
global.URLSearchParams = URLSearchParams;

// Sync router to store, as `store.state.route`.
sync(store, router);

// Http and Auth plugins
Vue.use(http);
Vue.use(auth);

// Vuetify
Vue.use(Vuetify, {
  theme: {
    primary: '#0A76DF',
    secondary: '#F5F5F5',
    accent: '#01BDF6',
  },
});

// Styles
require('./styles/scss/main.scss');
require('./styles/stylus/main.styl');

// Global Components
Vue.component('Appbar', Appbar);
Vue.component('Appfooter', Appfooter);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
