/**
 * Every route becomes a chunk, loaded only when used.
 * Reduces size of initial App load.
 */
const routes = [
  {
    name: 'login',
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */ '@/features/login/main.vue'),
    title: 'Login',
    layout: 'PublicLayout',
    isPublic: true,
  },
  {
    name: 'home',
    path: '/',
    component: () => import(/* webpackChunkName: "bids" */ '@/features/bids/main.vue'),
    title: 'Bids',
    layout: 'DefaultLayout',
    isPublic: false,
  },
  {
    name: 'bids',
    path: '/bids',
    component: () => import(/* webpackChunkName: "bids" */ '@/features/bids/main.vue'),
    title: 'Bids',
    layout: 'DefaultLayout',
    isPublic: false,
  },
  {
    name: 'account',
    path: '/account',
    component: () => import(/* webpackChunkName: "account" */ '@/features/account/main.vue'),
    title: 'Account',
    layout: 'DefaultLayout',
    isPublic: false,
  },
];

export default routes;
